%! program = pdflatex
\documentclass[12pt]{article}
\usepackage{geometry} 
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{amsmath,bm}
\usepackage[colorlinks=true,
            linkcolor=red,
            urlcolor=blue,
            citecolor=black]{hyper ref}
%\usepackage{url, multicol}
%\usepackage{MnSymbol}
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent

% style for partial derivative
\newcommand{\pd}[2]{ \frac{\partial #1}{\partial #2} }
%\newcommand{\pd}[2]{ \partial_{#2}#1 }
\newcommand{\od}[2]{\ensuremath{\frac{d #1}{d #2}}}
\newcommand{\td}[2]{\ensuremath{\frac{D #1}{D #2}}}
\newcommand{\ab}[1]{\ensuremath{\langle #1 \rangle}}
%\newcommand{\dab}[1]{\ensuremath{\llangle #1 \rrangle}}
\newcommand{\bss}[1]{\textsf{\textbf{#1}}}
\newcommand{\ol}{\ensuremath{\overline}}
\newcommand{\olx}[1]{\ensuremath{\overline{#1}^x}}
\newcommand{\nms}{\ensuremath{\mbox{ N m}^{-2}}}
\newcommand{\wms}{\ensuremath{\mbox{ W m}^{-2}}}
\newcommand{\mms}{\ensuremath{\mbox{ m}^2 \mbox{ s}^{-1}}}
\newcommand{\mss}{\ensuremath{\mbox{ m s}^{-2}}}
\newcommand{\ihat}{\hat{\textbf{\i}}}
\newcommand{\jhat}{\hat{\textbf{\j}}}
\newcommand{\orho}{\frac{1}{\rho_0}}

\geometry{letterpaper}

\title{ Revision of {\tt JPO-D-14-0160} \\  Reply to Reviewers}
\author{Ryan Abernathey \& Cimarron Wortham}
\date{} % delete this line to display the current date

\begin{document} \maketitle %\tableofcontents

\section*{Summary}

We thank the reviewers for their thoughtful feedback. We have addressed all their suggestions in our revision. This involved making changes to the discussion, references, and (following the suggestion of Reviewer 2), recalculating all of the results derived from the SST data to eliminate a potential aliasing issue. (None of the results are substantially changed, however.) We also added a new Fig.~1 illustrating the location of our sector of study on a map

We found the comments highly constructive, and the resulting manuscript appears improved. We hope you now find it suitable for publication. Below the comments are addressed point-by-pout.

\section*{Reviewer 1}

This reviewer recommend major revisions. % but didn't have any major suggestions!

\subsubsection*{Response to {\em General Comments and Recommendation}}

\begin{itemize}
\item {\em I first find the concept of an ``eddy'' phase speed to be confusing.} --- We have replaced the phrase with ``eddy propagation speed'' wherever possible.
\item {\em Since the authors only focus on the Eastern Pacific the title of the manuscript should reflect this rather than generally for the whole Pacific} --- Agreed. Title has been changed.
\item {\em I too find it a little hard to relate the findings to dynamical theories such as Ferrari and Nikurashin (2010)... The good correspondence with the model, which has other processes included in the velocity field, is encouraging.} --- This is one of the main reasons why we included the numerical model analysis in addition to the satellite observations. We are glad the reviewer found our argument convincing.
\item {\em I think that the dominate [sic.] scale of the cross-spectra is largely related to the size of westward propagating features.} --- This is consistent with what we have shown in our manuscript. We appreciate the physical insight into this mechanism provided by the reviewer. These kinematic arguments are well articulated by Killworth et al.~2004, which we have cited heavily.
\item {\em It would be really interesting to see this analysis done with ECCO2 to look at subsurface data, but that is beyond the scope of the current manuscript} --- This is certainly an appealing idea which we intend to pursue in the future. One challenge is that many modeling groups do not archive the interior fields from these new high-resolution GCMs with sufficient frequency to perform the cross-spectral analysis. Hopefully this paper will help persuade them to do so.
\item {\em Calculating cross-spectra from the satellite observations and comparing it to the climate simulation is a great new metric high-resolution climate simulations. I think the paper should focus more on this result.} --- We certainly agree with the reviewer on this point, and our original manuscript identifies model validation as a major goal of the work (l.~85 of original version). We have edited the introduction and abstract to make this even more prominent.
\end{itemize}

\subsubsection*{Response to {\em Minor Comments}}
\begin{enumerate}
\item {\em Line 24: ``or'' should be ``to''} --- Ok.
\item {\em It would be useful to have a figure of the E. Pacific sector.} --- We have added one as the new Fig.~1.
\item {\em Lines 199--203: You need to state where this equation (Eq.~1) comes from.} --- We have clarified the origin of Eq.~1.
\item {\em Lines 203--206: You also need to state assumptions in deriving the phase speed for a long baroclinic Rossby wave. Is it justified to use a time and depth-averaged zonal mean flow as the Doppler shift term?} --- We have clarified the assumptions and origins of this expression. Furthermore, a new paper has recently appeared (Klocker and Marshall, 2014) which puts this expression on firmer theoretical grounds.
\item {\em Line 207: Where does the 2 come from in the definition for Rhines scale?} --- This factor appears in Rhines' original paper; it originates in the linear Rossby wave dispersion relation for single-layer QG flow, $c = -\beta / (k^2 + l^2)$. The assumption of isotropy ($k^2 = l^2$) leads to a factor of two.
\item {\em Line 230: State where 50/360 comes from. I realize it is 1/4 x 200, but this should be explicitly stated. It wasn't obvious right away} --- No problem. Good suggestion.
\item {\em Eq.s 9 and 10: Why switch to a continuous form of the moment equation when thus far the equations were defined in discrete form? Be consistent.}  --- Agreed. Discrete notation is now employed.
\end{enumerate}

\section*{Reviewer 2}

This reviewer recommended minor revisions.

\subsubsection*{Response to {\em Detailed comments: Clarifications}}

\begin{enumerate}
\item {\em Line 132-33: The POP model is 1/10$^\circ$ grid spacing (not ``resolution'', see minor comment below) and what effective resolution of eddies and temperature anomalies that corresponds to is quite another thing. To help interpret your study it would be nice to know that the effective resolution is...Please address this.} --- We recognize the validity of this point and have done our best to address it. However, we are unaware of an established method for diagnosing ``effective resolution,'' and unfortunately the reviewer's comments do not provide any specific guidance. So we have decided to pursue further spectral analysis in order to characterize the effective resolution. One possible working definition of effective resolution is the wavelength below which the power spectra fall off sharply due to dissipation and grid-scale numerical effects, i.e.~the numerical Kolmogorov / Bachelor scales. In Fig.~\ref{fig:ps} of this document, we plot SST and surface velocity wavenumber power spectra from near 45N in the Pacific. No obvious steep drop-off is visible; however, both slopes steepen considerably at wavelength smaller than 40 km. This seems like a reasonable characterization of the effective resolution. We have revised the text to include this information.
\item {\em What fraction of the total power is contained in the phase speed spectrum between -1 and +1 m s$^{-1}$?} --- This is a good question whose answer is easily computed. The fraction of total power contained in this band is 95.5\% for the SST and 99.3\% for the meridional velocity. These numbers have been added to the text.
\end{enumerate}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=5.5in]{../figures/POP_spectrum.pdf}
\caption{Zonal wavenumber power spectra of SST and kinetic energy at 45N in the Pacific. }
\label{fig:ps}
\end{center}
\end{figure}

\subsubsection*{Response to {\em Interpretation (optional)}}

Here Reviewer 2 provides some very insightful comments on our results. In particular, he/she proposes that our results suggest that atmospheric damping of SST anomalies is relatively weak. We tend to agree; our perspective is that the anomalies are generated kinematically through stirring and dissipated through diffusion, not by atmospheric damping. We had viewed this predominantly as a question of time scale (rather than length scale), but clearly the time and length scales are related. We have opted to include the reviewer's insights in the discussion of Sec.~4 and the conclusions.

\subsubsection*{Response to {\em Minor comments}}

\begin{enumerate}
\item {\em Line 132-33 The POP model is described as 1/10? resolution. That's actually misleading. It's of course 1/10$^\circ$ grid spacing.} --- Agreed and corrected.
\item {\em Line 148-49. The daily SST data was sampled once per week. But this introduces aliasing into the SST spectra.} --- A very good point. We have recomputed our entire analysis following this suggestions. The daily SST data were smoothed in time with a 7-day boxcar filter and then subsampled. The results are basically unchanged. However, the difference is clearly visible in in $\ol{\Theta}(\omega)$ in Fig.~2. The smoothed data eliminated a substantial amount of high-frequency noise (which we can now attribute to aliasing) while leaving the energy-containing part of the spectrum unchanged. This strengthens our conclusions. We thank the reviewer for this valuable suggestion.
\item {\em Line 304. Eq. 9 defines the mean of k weighted by $\Theta^2$. So why do you say it only applies to the case of a gaussian? Similarly I think ``variance'' applies regardless of the shape of the spectrum.} --- True. That was phrased poorly. What we really meant is that a Gaussian is {\em completely} described by $M_1$ and $M_2$. We have revised the text accordingly.
\item {\em Typo in formula (your denominator = numerator).} --- This is not a typo. The numerator and denominator differ by the position of the absolute value operator (inside vs.~outside the integral).
\item {\em Line 317.  Typo: ``of the of the''.} --- Corrected.
\item {\em Line 334. Typo. ``also also''.} --- Corrected.
\item {\em Line 361--363. Awkward. } --- Corrected.
\item {\em Line 377. (and SST data)} --- Corrected.
\item {\em Line 425--426. Are velocities statistically isotropic? How do you know?} --- We examined statistics of $u$ as well as $v$ in the course of our analysis, although these results are not described in detail in the manuscript. That's how we know. Close to the equator, statistics are more anisotropic.
\item {\em How sensitive are your results to choices of thresholds for masking D?} --- Very insensitive. The masking simply eliminates singularities (where the gradient vanishes). A lower threshold masks larger regions, discarding potentially useful information, while a higher threshold results shows hyperbolically increasing values of $D$ near the zero-crossings. The value we picked provides a compromise between these two effects but is otherwise unimportant.
\item {\em Line 445: apparent contradiction} --- Ok, corrected.
\item {\em Line 478-79. There is growing evidence for the importance of bottom processes in dissipating eddies.} --- We have noted this and added some of the suggested references.
\item {\em Lines 493: What we learn from Scott and Furnival (2012) is that it?s fairly easy to estimate the currents in the upper 500m.} --- An encouraging comment to motivate continued study.
\end{enumerate}

\end{document}









